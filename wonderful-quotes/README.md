# Wonderful Quotes

> It's wonderful. You'll love it

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).


## Goals

- Build the app from scratch _without_ using the sample files provided on the course
- Use [Bootstrap 4](https://getbootstrap.com/) instead of 3
- Have a nice, clean design without much complexity to the exercise
- Explore some of the topics covered in the past (i.e: component communication, etc)
